//importing all the functions
let bestEconomyInSuperOver = require("../src/server/bestEconomyInSuperOver");
let dismissal = require("../src/server/dismissal");
let economicRate = require("../src/server/economicRate");
let extraRuns = require("../src/server/extraRuns");
let matchesPerTeamPerYear = require("../src/server/matchesPerTeamPerYear");
let matchesPerYear = require("../src/server/matchesPerYear");
let playerOfMatch = require("../src/server/playerOfMatch");
let strikeRate = require("../src/server/strikeRate");
let wonTossWonMatch = require("../src/server/wonTossWonMatch");

//executing all the functions
bestEconomyInSuperOver();
dismissal();
economicRate();
extraRuns();
matchesPerTeamPerYear();
matchesPerYear();
playerOfMatch();
strikeRate();
wonTossWonMatch();
