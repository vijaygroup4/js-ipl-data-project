//importing csv-parser,fs and path modules
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

//file paths
const deliveriesFilePath = path.join(__dirname, "../data/deliveries.csv");
const dumpPath = path.join(
  __dirname,
  "../public/output/bestEconomyInSuperOver.json"
);

//function that return best economy bowler in super over
function bestEconomyInSuperOver() {
  let deliveriesData = [];
  //reading the deliveries csv data file using fs and csv-parser
  fs.createReadStream(deliveriesFilePath)
    .pipe(csv())
    .on("data", (data) => {
      deliveriesData.push(data);
    })
    .on("error", (error) => {
      console.log(error);
    })
    .on("end", () => {
      try {
        let newObject = {};
        //iterating through deliveries data and checking for super overs
        for (let object of deliveriesData) {
          let isSuperOver = object.is_super_over;
          let bowler = object.bowler;
          let totalRuns = object.total_runs;
          //if super over is true storing the result in newObject
          if (isSuperOver == 1) {
            if (newObject.hasOwnProperty(bowler)) {
              newObject[bowler].runs += parseInt(totalRuns);
              newObject[bowler].balls += 1;
            } else {
              newObject[bowler] = {
                runs: parseInt(totalRuns),
                balls: 1,
              };
            }
          }
        }

        //calculating the economy rate
        for (let key in newObject) {
          newObject[key].economy =
            newObject[key].runs / (newObject[key].balls / 6);
        }

        let keysArray = Object.keys(newObject);
        //sorting the bowlers based on economy rate
        let sortedKeys = keysArray.sort(function (key1, key2) {
          return newObject[key1].economy - newObject[key2].economy;
        });

        let finalOutput = [sortedKeys[0], newObject[sortedKeys[0]]];

        //dumping the result to output folder
        fs.writeFile(dumpPath, JSON.stringify(finalOutput), (error) => {
          if (error) {
            console.log(error);
          }
        });
      } catch (error) {
        console.log(error);
      }
    });
}

//exporting the above function
module.exports = bestEconomyInSuperOver;
