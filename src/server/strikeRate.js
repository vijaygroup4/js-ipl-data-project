//importing csv,fs and path modules
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

//creating file paths
const matchesFilePath = path.join(__dirname, "../data/matches.csv");
const deliveriesFilePath = path.join(__dirname, "../data/deliveries.csv");
const dumpPath = path.join(__dirname, "../public/output/strikeRate.json");

//function that returns strike rate of each player per each year
function strikeRate() {
  let matchesData = [];
  let deliveriesData = [];

  // reading matches csv data
  fs.createReadStream(matchesFilePath)
    .pipe(csv())
    .on("data", (data) => {
      matchesData.push(data);
    })
    .on("error", (error) => {
      console.log(error);
    })
    .on("end", () => {
      try {
        let idsObject = {};
        //iterating through matches data
        for (let object of matchesData) {
          if (idsObject.hasOwnProperty(object.season)) {
            idsObject[object.season].push(object.id);
          } else {
            idsObject[object.season] = [object.id];
          }
        }

        // reading deliveries csv data
        fs.createReadStream(deliveriesFilePath)
          .pipe(csv())
          .on("data", (data) => {
            deliveriesData.push(data);
          })
          .on("error", (error) => {
            console.log(error);
          })
          .on("end", () => {
            let resultObject = {};

            // iterating over seasons
            for (let key in idsObject) {
              resultObject[key] = {};

              // iterating over deliveries data and if ids matches then storing the batsman runs and balls
              for (let object of deliveriesData) {
                if (idsObject[key].includes(object.match_id)) {
                  if (resultObject[key].hasOwnProperty(object.batsman)) {
                    resultObject[key][object.batsman].runs += parseInt(
                      object.batsman_runs
                    );
                    resultObject[key][object.batsman].balls += 1;
                  } else {
                    resultObject[key][object.batsman] = {
                      runs: parseInt(object.batsman_runs),
                      balls: 1,
                    };
                  }
                }
              }
            }

            // calculating the strike rate for each batsman
            for (let season in resultObject) {
              for (let batsman in resultObject[season]) {
                const { runs, balls } = resultObject[season][batsman];
                resultObject[season][batsman].strikeRate = (runs / balls) * 100;
              }
            }

            //dumping the result to output folder
            fs.writeFile(dumpPath, JSON.stringify(resultObject), (error) => {
              if (error) {
                console.log(error);
              }
            });
          });
      } catch (error) {
        console.log(error);
      }
    });
}

//exporting the above function
module.exports = strikeRate;
