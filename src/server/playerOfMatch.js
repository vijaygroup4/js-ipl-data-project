//importing csv,fs and path modules
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

//creating file paths
const matchFilePath = path.join(__dirname, "../data/matches.csv");
const dumpPath = path.join(__dirname, "../public/output/playerOfMatch.json");

//function that return player of match per season
function playerOfMatch() {
  let matchesData = [];
  //reading matches csv data
  fs.createReadStream(matchFilePath)
    .pipe(csv())
    .on("data", (data) => {
      matchesData.push(data);
    })
    .on("error", (error) => {
      console.log(error);
    })
    .on("end", () => {
      try {
        let newObject = {};
        //iterating through matches data and finding the player per each season
        for (let object of matchesData) {
          if (newObject.hasOwnProperty(object.season)) {
            if (
              newObject[object.season].hasOwnProperty(object.player_of_match)
            ) {
              newObject[object.season][object.player_of_match] += 1;
            } else {
              newObject[object.season][object.player_of_match] = 1;
            }
          } else {
            newObject[object.season] = {};
          }
        }

        //extracting the keys from newObject
        let keysArray = Object.keys(newObject);
        let valuesArray = Object.values(newObject);
        let finalArray = [];
        //iterating through objects and finding top most player
        for (let object of valuesArray) {
          let randomPlayer;
          let randomValue = 0;
          for (let key in object) {
            if (object[key] > randomValue) {
              randomValue = object[key];
              randomPlayer = key;
            }
          }
          finalArray.push(randomPlayer);
        }

        let finalObject = {};
        //collecting the player as value and key as year
        //shift gives the first player name in array each time
        for (let key of keysArray) {
          finalObject[key] = finalArray.shift();
        }

        //dumping the result to output folder
        fs.writeFile(dumpPath, JSON.stringify(finalObject), (error) => {
          if (error) {
            console.log(error);
          }
        });
      } catch (error) {
        console.log(error);
      }
    });
}

//exporting the above function
module.exports = playerOfMatch;
