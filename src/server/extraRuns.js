//importing csv,fs and path modules
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

//creating the file paths
const matchFilePath = path.join(__dirname, "../data/matches.csv");
const deliveriesFilePath = path.join(__dirname, "../data/deliveries.csv");
const dumpPath = path.join(__dirname, "../public/output/extraRuns.json");

//function that gives extra runs for each team
function extraRuns() {
  let matchesData = [];
  //reading the matches csv data
  fs.createReadStream(matchFilePath)
    .pipe(csv())
    .on("data", (data) => {
      matchesData.push(data);
    })
    .on("error", (error) => {
      console.log(error);
    })
    .on("end", () => {
      try {
        let newObject = {};
        let newArray = [];
        //iterating through matches data and collecting ids of 2016
        for (let object of matchesData) {
          if (object.season === "2016") {
            newArray.push(object.id);
          }
        }

        let deliveriesData = [];
        //reading the deliveries csv data
        fs.createReadStream(deliveriesFilePath)
          .pipe(csv())
          .on("data", (data) => {
            deliveriesData.push(data);
          })
          .on("error", (error) => {
            console.log(error);
          })
          .on("end", () => {
            //iterating through the ids
            for (let id of newArray) {
              //iterating through deliveries data
              for (let object of deliveriesData) {
                //filtering for 2016 matches
                if (id === object.match_id) {
                  if (newObject.hasOwnProperty(object.batting_team) === false) {
                    newObject[object.batting_team] = parseFloat(
                      object["extra_runs"]
                    );
                  } else {
                    newObject[object.batting_team] += parseFloat(
                      object["extra_runs"]
                    );
                  }
                }
              }
            }

            //dumping the result to output folder
            fs.writeFile(dumpPath, JSON.stringify(newObject), (error) => {
              if (error) {
                console.log(error);
              }
            });
          });
      } catch (error) {
        console.log(error);
      }
    });
}

//exporting the above function
module.exports = extraRuns;
