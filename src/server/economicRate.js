//importing csv,fs and path modules
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

//creating file paths
const matchFilePath = path.join(__dirname, "../data/matches.csv");
const deliveriesFilePath = path.join(__dirname, "../data/deliveries.csv");
const dumpPath = path.join(__dirname, "../public/output/economicRate.json");

//function that return top 10 economical bowlers
function economicRate() {
  let matchesData = [];
  let deliveriesData = [];
  //reading the matches csv data
  fs.createReadStream(matchFilePath)
    .pipe(csv())
    .on("data", (data) => {
      matchesData.push(data);
    })
    .on("error", (error) => {
      if (error) {
        console.log(error);
      }
    })
    .on("end", () => {
      try {
        let idsArray = [];
        //iterating through matchesData and filtering the ids of 2015
        for (let object of matchesData) {
          if (object.season === "2015") {
            idsArray.push(object.id);
          }
        }

        //reading the deliveries csv data
        fs.createReadStream(deliveriesFilePath)
          .pipe(csv())
          .on("data", (data) => {
            deliveriesData.push(data);
          })
          .on("error", (error) => {
            if (error) {
              console.log(error);
            }
          })
          .on("end", (end) => {
            let actualObjects = [];
            //iterating through deliveries data and if ids are equal,then searching for bowlers
            for (let object of deliveriesData) {
              if (idsArray.includes(object.match_id)) {
                actualObjects.push(object);
              }
            }

            let ballsObject = {};
            let totalRunsObject = {};
            //iterating through actual objects and counting total runs and balls thrown
            for (let object of actualObjects) {
              if (ballsObject[object.bowler]) {
                ballsObject[object.bowler]++;
              } else {
                ballsObject[object.bowler] = 1;
              }
              //counting the total runs
              if (totalRunsObject[object.bowler]) {
                totalRunsObject[object.bowler] += parseInt(object.total_runs);
              } else {
                totalRunsObject[object.bowler] = parseInt(object.total_runs);
              }
            }

            let finalObject = {};
            //collecting common keys from both objects and creating array
            let commonKeysArray = Object.keys(totalRunsObject);
            for (let key of commonKeysArray) {
              finalObject[key] = (totalRunsObject[key] * 6) / ballsObject[key];
            }

            let finalObjectKeys = Object.keys(finalObject);
            //sorting the bowlers
            finalObjectKeys.sort((bowler1, bowler2) => {
              return finalObject[bowler1] - finalObject[bowler2];
            });

            //taking only top 10 bolwers
            let finalOutput = finalObjectKeys.slice(0, 10);

            //pushing the result into output folder
            fs.writeFileSync(dumpPath, JSON.stringify(finalOutput), (error) => {
              if (error) {
                console.log(error);
              }
            });
          });
      } catch (error) {
        console.log(error);
      }
    });
}

//exporting the above function
module.exports = economicRate;
