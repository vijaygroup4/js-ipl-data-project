//importing csv,fs and path modules
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

//creating file paths
const matchesFilePath = path.join(__dirname, "../data/matches.csv");
const dumpPath = path.join(
  __dirname,
  "../public/output/matchesPerTeamPerYear.json"
);

//function that returns total matches per team per year
function matchesPerTeamPerYear() {
  let matchesData = [];
  //reading matches csv data
  fs.createReadStream(matchesFilePath)
    .pipe(csv())
    .on("data", (data) => {
      matchesData.push(data);
    })
    .on("error", (error) => {
      console.log(error);
    })
    .on("end", () => {
      try {
        let newObject = {};
        //iterating through matches data and counting no of matches per team
        for (let object of matchesData) {
          let winner = object.winner;
          let year = object.season;

          if (newObject.hasOwnProperty(year) === false) {
            newObject[year] = {};
          }
          if (newObject[year].hasOwnProperty(winner) === false) {
            newObject[year][winner] = 1;
          } else {
            newObject[year][winner] += 1;
          }
        }

        //dumping the result to output folder
        fs.writeFile(dumpPath, JSON.stringify(newObject), (error) => {
          if (error) {
            console.log(error);
          }
        });
      } catch (error) {
        console.log(error);
      }
    });
}

//exporting the above function
module.exports = matchesPerTeamPerYear;
