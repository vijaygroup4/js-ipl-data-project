//importing csv,fs and path modules
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

//creating filepaths using path module
const deliveriesFilePath = path.join(__dirname, "../data/deliveries.csv");
const dumpPath = path.join(__dirname, "../public/output/dismissal.json");

//function that return who dismissed by another player more times
function dismissal() {
  let deliveriesData = [];
  //reading the deliveries csv data
  fs.createReadStream(deliveriesFilePath)
    .pipe(csv())
    .on("data", (data) => {
      deliveriesData.push(data);
    })
    .on("error", (error) => {
      console.log(error);
    })
    .on("end", () => {
      try {
        let newObject = {};
        //iterating through deliveries data
        for (let object of deliveriesData) {
          let batsman = object.batsman;
          let bowler = object.bowler;
          let dismissalKind = object.dismissal_kind;

          //if dismissalKind is true the storing the batsman and bowler
          if (dismissalKind) {
            if (newObject.hasOwnProperty(batsman)) {
              if (newObject[batsman].hasOwnProperty(bowler)) {
                newObject[batsman][bowler] += 1;
              } else {
                newObject[batsman][bowler] = 1;
              }
            } else {
              newObject[batsman] = {};
              newObject[batsman][bowler] = 1;
            }
          }
        }

        //getting the keys of newObject into array
        let keysArray = Object.keys(newObject);
        let valuesArray = Object.values(newObject);

        let finalObject = {};
        //iterating the bowlers and finding who dismissed more times
        for (let key of keysArray) {
          let randomValue = 0;
          let randomKey;
          for (let innerKey in newObject[key]) {
            if (newObject[key][innerKey] > randomValue) {
              randomValue = newObject[key][innerKey];
              randomKey = innerKey;
            }
          }
          finalObject[key] = {
            bowler: randomKey,
            occurences: randomValue,
          };
        }

        //dumping the result to output folder
        fs.writeFile(dumpPath, JSON.stringify(finalObject), (error) => {
          if (error) {
            console.log(error);
          }
        });
      } catch (error) {
        console.log(error);
      }
    });
}

//exporting the above function
module.exports = dismissal;
