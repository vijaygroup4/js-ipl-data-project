//importing csv,fs and path modules
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

//creating the file paths
const matchesFilePath = path.join(__dirname, "../data/matches.csv");
const dumpPath = path.join(__dirname, "../public/output/wonTossWonMatch.json");

//function that return the teams that won the toss and match
function wonTossWonMatch() {
  let matchesData = [];
  //reading matches csv data
  fs.createReadStream(matchesFilePath)
    .pipe(csv())
    .on("data", (data) => {
      matchesData.push(data);
    })
    .on("error", (error) => {
      console.log(error);
    })
    .on("end", () => {
      try {
        let newObject = {};

        //iterating through matches data
        for (let object of matchesData) {
          let winner = object.winner;
          let tossWinner = object.toss_winner;

          //if team wins the toss and match,then storing his details
          if (winner === tossWinner) {
            if (newObject.hasOwnProperty(winner)) {
              newObject[winner] += 1;
            } else {
              newObject[winner] = 1;
            }
          }
        }

        //dumping the result to output folder
        fs.writeFile(dumpPath, JSON.stringify(newObject), (error) => {
          if (error) {
            console.log(error);
          }
        });
      } catch (error) {
        console.log(error);
      }
    });
}

//exporting the above function
module.exports = wonTossWonMatch;
