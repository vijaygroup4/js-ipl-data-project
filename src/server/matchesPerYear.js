//importing csv,fs and path modules
const csv = require("csv-parser");
const fs = require("fs");
const path = require("path");

//creating file paths
const matchesFilePath = path.join(__dirname, "../data/matches.csv");
const dumpPath = path.join(__dirname, "../public/output/matchesPerYear.json");

//function that return matches per year
function matchesPerYear() {
  let matchesData = [];
  //reading the matches csv data
  fs.createReadStream(matchesFilePath)
    .pipe(csv())
    .on("data", (data) => {
      matchesData.push(data);
    })
    .on("error", (error) => {
      console.log(error);
    })
    .on("end", () => {
      try {
        let newObject = {};
        //iterating through matches data and counting matches per year
        for (let object of matchesData) {
          if (newObject.hasOwnProperty(object.season)) {
            newObject[object.season] += 1;
          } else {
            newObject[object.season] = 1;
          }
        }

        //dumping the result to output folder
        fs.writeFile(dumpPath, JSON.stringify(newObject), (error) => {
          if (error) {
            console.log(error);
          }
        });
      } catch (error) {
        console.log(error);
      }
    });
}

//exporting the above function
module.exports = matchesPerYear;
